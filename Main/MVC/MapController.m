//
//  MapController.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-05.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "MapController.h"
#import "GeoService.h"
#import "AppData.h"
#import "Place.h"
#import "Level.h"
#import "JsonParser.h"
#import "Pnt.h"
#import "Path.h"
#import "PlaceListView.h"
#import "LookupPlace.h" 

@implementation MapController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self initialize];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) changeZoom:(NSInteger)delta {
  GeoPoint pnt;
  Level *lvl;
  if(delta==0||currentZoom==0) {
    pnt = GeoPointMake(appData.lon, appData.lat);
    
    lvl=[appData getCurrent];
  }
  else {
    pnt = [self midPoint];
    if(delta==-1) {
      lvl=[appData goPrevious];
    }
    else {
      lvl=[appData goNext];
    }
  }
  [self doRender:lvl lon:pnt.lon lat:pnt.lat];
}


- (void) doRender:(Level *)lvl lon:(double)lon lat:(double)lat {
  currentZoom=lvl.zoom;
  tileNumber=lvl.tileNumber;
  tileSize=lvl.tileSize;
  shiftHorizon=tileSize/2-deviceWidth/2;
  shiftVertical=tileSize/2-deviceHeight/2;
  deltaHorizonl=(tileSize-deviceWidth)/8;
  deltaVertical=(tileSize-deviceHeight)/8;
  imageNamePrefix=lvl.pathPrefix;
  _mapPanel.frame=CGRectMake(0,0,tileSize,tileSize);
  WsgPoint mid=[GeoUtil toWsgWithZoom:currentZoom longitude:appData.lon  latitude:appData.lat];
  middleX=mid.xPos;
  middleY=mid.yPos;
  GeoPoint p= [GeoUtil toGeoWithZoom:currentZoom xcoord:middleX ycoord:middleY];
  dlon=appData.lon-p.lon;
  dlat=appData.lat-p.lat;
  WsgPoint actual=[GeoUtil toWsgWithZoom:currentZoom longitude:lon latitude:lat];
  [self createMapTile:TRUE x:actual.xPos-middleX+tileNumber*tileSize/2 y:actual.yPos-middleY+tileNumber*tileSize/2];
  _mapPanel.image = current;
  _zoomUp.enabled=appData.hasNextLevel;
  _zoomDown.enabled=appData.hasPreviousLevel;
   [self relocate];
}

-(void) initialize {
  NSError *error;
  NSString *bundleRoot= [[NSBundle mainBundle]bundlePath];
  NSArray *dirContents= [[NSFileManager defaultManager]
                         contentsOfDirectoryAtPath:bundleRoot error:&error];
  navigating=FALSE;
  service=[[GeoService alloc] initWithMain:self ];
  
  locations=[[NSMutableArray alloc] init];
  appData=[JsonParser readAppData];
  deviceWidth=320;
  deviceHeight=480;
  [self changeZoom:0];
  UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
  [self.view addGestureRecognizer:tap];
  UIPanGestureRecognizer * pan = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
  [self.view addGestureRecognizer:pan];
  
  _spin.hidden=YES;
  [_spin hidesWhenStopped];
  //[_spin startAnimating];
  
}

- (void) reloadAppData {
  appData=[JsonParser readAppData];
}

- (void) relocate {
  _mapPanel.center=CGPointMake(deviceWidth/2+xTileCenter-xWsg, deviceHeight/2+yTileCenter-yWsg);
}

- (IBAction)zoomIn:(id)sender {
  [self changeZoom:1];
}

- (IBAction)zoomOut:(id)sender {
  [self changeZoom:-1];
}

- (void) spotSelected:(Place*) spot {
  NSLog(@"%@ is selected.",spot.name);
}

- (void) selectedLon:(double) lon lat:(double)lat {
  NSString *message =[NSString stringWithFormat:@"Location (%8.4f,%8.4f)",lon,lat];
  [self showDialog:3 message:message];
}

- (void) showDialog:(NSInteger) duration message:(NSString*) message {
  UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                  message:message
                                                 delegate:nil
                                        cancelButtonTitle:nil
                                        otherButtonTitles:nil, nil];
  [toast show];
   dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    [toast dismissWithClickedButtonIndex:0 animated:YES];
  });

}

-(void)handleTap:(UITapGestureRecognizer *)recognizer {
  CGPoint pnt = [recognizer locationInView:self.view];
  if(pnt.y>deviceHeight-40) {
    return;
  }
  CGPoint location = [recognizer locationInView:_mapPanel];
  for(id item in appData.places) {
    Place *sp=(Place *)item;
    if(sp.x>-32&&sp.y>-32) {
      if(fabs(sp.x-location.x)<16&&fabs(sp.y-location.y)<16) {
        [self spotSelected:sp];
        return;
      }
    }
  }
  NSInteger x=middleX+(int)location.x-tileSize/2;
  NSInteger y=middleY+(int)location.y-tileSize/2;
  GeoPoint p=[GeoUtil toGeoWithZoom:currentZoom xcoord:x ycoord:y];
  [self selectedLon:p.lon+dlon lat:p.lat+dlat];
}

-(void)handlePan:(UIPanGestureRecognizer *)recognizer {
  if (recognizer.state == UIGestureRecognizerStateEnded) {
    int fx = xWsg;
    int fy = yWsg;
    CGPoint offset = [recognizer translationInView:self.view];
    int xx = xWsg - (int)offset.x;
    int yy = yWsg - (int)offset.y;
    if(abs(xx-xTileCenter)>shiftHorizon||abs(yy-yTileCenter)>shiftVertical) {
      if(![self createMapTile:FALSE x:xx y:yy]) {
        //NSLog(@"Reload Map Image");
        _mapPanel.image = current;
      }
    }
    else {
      xWsg = xx;
      yWsg = yy;
      
    }
    // NSLog(@"Drag at with %f, %f (%d, %d)/(%d %d)",-offset.x,-offset.y, xWsg, yWsg, fx,fy);
    [self relocate];
  }
  
}


- (IBAction)startNavagation:(id)sender {
  if (navigating) {
    navigating = FALSE;
    [service stop];
    UIImage *mv = [UIImage imageNamed:@"Resume0.png"];
    _actionBtn.imageView.image=mv;
    [locations removeAllObjects];
    [self createMapTile:TRUE x:xWsg y:yWsg];
    _mapPanel.image = current;
    [self relocate];
  }
  else {
    //NSLog(@"Start Navigating");
    _actionBtn.enabled=NO;
    _spin.hidden=NO;
    [_spin startAnimating];
    [service start];
  }
 
}

- (IBAction)managePlaces:(id)sender {
  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  PlaceListView *placeManager=(PlaceListView *)[mainStoryboard instantiateViewControllerWithIdentifier:@"PlaceManager"];
   [placeManager managePlaces:self appData:appData];
   [self.navigationController pushViewController:placeManager animated:YES];
}

- (IBAction)lookupPlace:(id)sender {
  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  LookupPlace *placeManager=(LookupPlace *)[mainStoryboard instantiateViewControllerWithIdentifier:@"LookupPlace"];
  [placeManager lookupPlace:self data:appData];
  [self.navigationController pushViewController:placeManager animated:YES];
}

- (BOOL)createMapTile:(BOOL)create x:(NSInteger)x y:(NSInteger)y;
{
  if(tileNumber==1) {
      xWsg = ((x<deviceWidth/2))?deviceWidth/2:(x>tileSize-deviceWidth/2)?tileSize-deviceWidth/2:x;
      yWsg = ((y<deviceHeight/2))?deviceHeight/2:(y>tileSize-deviceHeight/2)?tileSize-deviceHeight/2:y;
      return [self pickCell:create row:0 column:0];
  }
  int min=tileSize/2;
  int max=tileNumber*tileSize-min;
  NSInteger centerx=x<min?min:x>max?max:x;
  NSInteger centery=y<min?min:y>max?max:y;
  min=deviceWidth/2;
  max=tileNumber*tileSize-min;
  xWsg=x<min?min:x>max?max:x;
  min=deviceHeight/2;
  max=tileNumber*tileSize-min;
  yWsg=y<min?min:y>max?max:y;
  //NSLog(@"curret %d  %d  truncated %d  %d  new center %d  %d",x, y,xWsg, yWsg, centerx, centery);
  if(centerx==xTileCenter&&centery==yTileCenter&&!create) {
    return TRUE;
  }
  int column=1;
  for(int i=0; i<tileNumber; i++) {
    int mx=i*tileSize+tileSize/2;
    //NSLog(@" i=%d mx=%d center=%d",i,mx,centerx);
    if(abs(mx-centerx)<=deltaHorizonl) {
      for(int j=0; j<tileNumber; j++) {
        int my=j*tileSize+tileSize/2;
        if(abs(my-centery)<=deltaVertical) {
          return [self pickCell:create row:j column:i];
        }
      }
      return [self pickColumn:create column:i y:centery];
    }
    else if(mx>centerx) {
      column=i;
      break;
    }
  }
  int row=1;
  for(int j=0; j<tileNumber; j++) {
    int my=j*tileSize+tileSize/2;
    if(abs(my-centery)<=deltaVertical) {
      return [self pickRow:create row:j x:centerx];
    }
    else if(my>centery) {
      row=j;
      break;
    }
  }
 
  if(centerx==xTileCenter&&centery==yTileCenter&&!create) {
    return TRUE;
  }
  xTileCenter=centerx;
  yTileCenter=centery;
  // NSLog(@"%d, %d / %d, %d",row, column, centerx, centery);
  int w = column*tileSize+tileSize/2-xTileCenter;
  int h = row*tileSize+tileSize/2-yTileCenter;
  UIImage *nw=[self subImage:CGRectMake(tileSize-w,tileSize-h,w,h) row:row-1 column:column-1];
  UIImage *ne=[self subImage:CGRectMake(0,tileSize-h,tileSize-w,h) row:row-1 column:column];
  UIImage *sw=[self subImage:CGRectMake(tileSize-w,0,w,tileSize-h) row:row column:column-1];
  UIImage *se=[self subImage:CGRectMake(0,0,tileSize-w,tileSize-h) row:row column:column];
  UIGraphicsBeginImageContext(CGSizeMake(tileSize, tileSize));
  [nw drawInRect:CGRectMake(0, 0, w, h)];
  [ne drawInRect:CGRectMake(w, 0, tileSize-w, h)];
  [sw drawInRect:CGRectMake(0, h, w, tileSize-h)];
  [se drawInRect:CGRectMake(w, h, tileSize-w, tileSize-h)];
  [self drawOverlay];
  current = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return FALSE;
}

- (BOOL)pickColumn:(BOOL)create column:(NSInteger)column y: (NSInteger)y {
  NSInteger x=column*tileSize+tileSize/2;
  if(x==xTileCenter&&y==yTileCenter&&!create) {
    return TRUE;
  }
  xTileCenter=x;
  yTileCenter=y;
  int row=1;
  while(row*tileSize+tileSize/2<yTileCenter) row++;
  int h=tileSize/2-yTileCenter+row*tileSize;
  UIImage *top = [self subImage:CGRectMake(0,tileSize-h,tileSize,h) row:row-1 column:column];
  UIImage *bottom = [self subImage:CGRectMake(0,0,tileSize,tileSize-h) row:row column:column];
  UIGraphicsBeginImageContext(CGSizeMake(tileSize, tileSize));
  [top drawInRect:CGRectMake(0, 0, tileSize, h)];
  [bottom drawInRect:CGRectMake(0, h, tileSize, tileSize-h)];
  [self drawOverlay];
  current = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return FALSE;
}

- (BOOL)pickRow:(BOOL)create row:(NSInteger)row x: (NSInteger)x{
  NSInteger y=row*tileSize+tileSize/2;
  if(x==xTileCenter&&y==yTileCenter&&!create){
      return TRUE;
  }
  xTileCenter = x;
  yTileCenter = y;
  int column=1;
  while(column*tileSize+tileSize/2<xTileCenter) column++;
  int w=column*tileSize+tileSize/2-xTileCenter;
  UIImage *left = [self subImage:CGRectMake(tileSize-w, 0,w, tileSize) row:row column:column-1];
  UIImage *right = [self subImage:CGRectMake(0, 0,tileSize-w,tileSize) row:row column:column];
  UIGraphicsBeginImageContext(CGSizeMake(tileSize, tileSize));
  [left drawInRect:CGRectMake(0, 0, w, tileSize)];
  [right drawInRect:CGRectMake(w, 0, tileSize-w, tileSize)];
  [self drawOverlay];
  current = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return FALSE;
}

- (BOOL)pickCell:(BOOL)create row:(NSInteger)row column: (NSInteger)column {
  NSInteger x=column*tileSize+tileSize/2;
  NSInteger y=row*tileSize+tileSize/2;
  if(x==xTileCenter&&y==yTileCenter&&!create){
    return TRUE;
  }
  xTileCenter=x;
  yTileCenter=y;
  
  UIGraphicsBeginImageContext(CGSizeMake(tileSize, tileSize));
  NSString *name = [self imageNameTileX:column TileY:row];
  UIImage *img = [UIImage imageNamed:name];
  [img drawInRect:CGRectMake(0, 0, tileSize, tileSize)];
  [self drawOverlay];
  current = UIGraphicsGetImageFromCurrentImageContext();
  
  UIGraphicsEndImageContext();
  return FALSE;
}

-(UIImage *) subImage:(CGRect)rect row:(NSInteger)row column:(NSInteger)column  {
  //NSLog(@"Load Image %@",[self imageNameTileX:column TileY:row]);
  UIImage *tile=[UIImage imageNamed:[self imageNameTileX:column TileY:row]];
  CGImageRef imageRef = CGImageCreateWithImageInRect([tile CGImage], rect);
  UIImage *subTile = [UIImage imageWithCGImage:imageRef];
  CGImageRelease(imageRef);
  return subTile;
}

- (void) drawTestRoute :(CGContextRef)context{
  int ox = -middleX + tileSize/2 - xTileCenter + tileSize*tileNumber/2;
  int oy = -middleY + tileSize/2 - yTileCenter + tileSize*tileNumber/2;
  NSMutableArray *xy=[[NSMutableArray alloc] init];
  NSDate *now =[NSDate date];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-330 ] lon:-80.559793 lat: 43.494629] ];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-280 ] lon:-80.561376 lat: 43.494177] ];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-240 ] lon:-80.563334 lat: 43.493656] ];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-200 ] lon:-80.564439 lat: 43.493570] ];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-120 ] lon:-80.564455 lat: 43.491243] ];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-60 ] lon:-80.560517 lat: 43.4926879] ];
  [xy addObject:[[Pnt alloc] initWithDate:[now dateByAddingTimeInterval:-5 ] lon:-80.561376 lat: 43.494177] ];
  NSInteger mn =[xy count];
  CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
  Pnt *pp =[xy objectAtIndex:0];
  WsgPoint ww=[GeoUtil toWsgWithZoom:currentZoom longitude:pp.lon latitude:pp.lat];
  NSInteger xx = ww.xPos+ox;
  NSInteger yy = ww.yPos+oy;
  CGContextSetLineWidth(context, currentZoom<14?1.2:2.5);
  CGContextStrokeEllipseInRect(context, CGRectMake(xx-3,yy-3,6,6));
  CGContextMoveToPoint(context, xx, yy);
  for(int i=1; i<mn; i++) {
    pp = [xy objectAtIndex:i];
    ww = [GeoUtil toWsgWithZoom:currentZoom longitude:pp.lon latitude:pp.lat];
    xx = ww.xPos + ox;
    yy = ww.yPos + oy;
    CGContextAddLineToPoint(context, xx, yy);
  }
  CGContextStrokePath(context);
}

-(void) drawOverlay {
  int cnt = 0;
  int ox = -middleX + tileSize/2 - xTileCenter + tileSize*tileNumber/2;
  int oy = -middleY + tileSize/2 - yTileCenter + tileSize*tileNumber/2;
  for (id spotItem in appData.places) {
    Place *sp=(Place *)spotItem;
    sp.x = -100;
    sp.y = -100;
    //NSString *path=sp.logoPath;
    if(sp.logoPath) {
      WsgPoint w = [GeoUtil toWsgWithZoom:currentZoom longitude:sp.lon latitude:sp.lat];
      int x = w.xPos + ox;
      if(x>-32 && x<tileSize+32) {
        int y = w.yPos+oy;
        if(y>-32 && y <tileSize+32) {
          sp.x = x;
          sp.y = y;
          int lvl = [appData getCurrent].level;
          UIImage *image;
          if(lvl<2) {
            NSString *path = [NSString stringWithFormat:@"%@%d.png",@"pin",cnt%9];
            image = [UIImage imageNamed:path];
            cnt++;
          }
          else {
            image=[GeoUtil imageForPath:sp.logoPath];
          }
          CGSize size = image.size;
          [image drawInRect:CGRectMake(x-size.width/2, y-size.height/2, size.width, size.height)];
        }
      }
    }
  }
  if(navigating && [locations count]>0) {
    NSInteger n =[locations count];
    //NSLog(@"Draw Overlay %d",(int)n);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    Pnt *p = [locations objectAtIndex:0];
    WsgPoint w=[GeoUtil toWsgWithZoom:currentZoom longitude:p.lon latitude:p.lat];
    NSInteger x = w.xPos+ox;
    NSInteger y = w.yPos+oy;
    if(n==1) {
      CGContextSetLineWidth(context, 3.0);
      CGContextStrokeEllipseInRect(context, CGRectMake(x-10,y-10,20,20));
    }
    else {
      CGContextSetLineWidth(context, currentZoom<14?1.2:2.5);
      CGContextStrokeEllipseInRect(context, CGRectMake(x-3,y-3,6,6));
      CGContextMoveToPoint(context, x, y);
      for(int i=1; i<n; i++) {
         p = [locations objectAtIndex:i];
         w = [GeoUtil toWsgWithZoom:currentZoom longitude:p.lon latitude:p.lat];
         x = w.xPos + ox;
         y = w.yPos + oy;
         CGContextAddLineToPoint(context, x, y);
      }
      CGContextStrokePath(context);
    }
  }
  if(target != nil)  {
    CGContextRef context = UIGraphicsGetCurrentContext();
    WsgPoint ww = [GeoUtil toWsgWithZoom:currentZoom longitude:target.lon latitude:target.lat];
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextFillEllipseInRect(context, CGRectMake(ww.xPos + ox-4.5, ww.yPos + oy-4.5,9,9));

    if(navigating && [locations count]>0) {
      CGContextSetLineWidth(context, currentZoom<14?1.2:2.5);
      CGContextSetStrokeColorWithColor(context, [UIColor greenColor].CGColor);
      CGContextMoveToPoint(context, ww.xPos+ox,ww.yPos+oy);
      Pnt *pnt = [locations lastObject];
      ww = [GeoUtil toWsgWithZoom:currentZoom longitude:pnt.lon latitude:pnt.lat];
      CGContextAddLineToPoint(context, ww.xPos+ox,ww.yPos+oy);
      CGContextStrokePath(context);
      CGContextFillEllipseInRect(context, CGRectMake(ww.xPos + ox-3.5, ww.yPos + oy-3.5,7,7));
    }
  }
  
}

- (GeoPoint) midPoint {
  GeoPoint gp = [GeoUtil toGeoWithZoom:currentZoom xcoord:xWsg+middleX-tileNumber*tileSize/2 ycoord:yWsg+middleY-tileNumber*tileSize/2];
  return GeoPointMake(gp.lon+dlon ,gp.lat+dlat);
}

- (GeoPoint) refPoint {
  if(navigating && [locations count]>0) {
    Pnt *p =(Pnt*)[locations lastObject];
    return GeoPointMake(p.lon, p.lat);
  }
  return [self midPoint];
}

-(NSString *) imageNameTileX:(NSInteger)xx TileY:(NSInteger)yy {
  return  [NSString stringWithFormat:@"%@%d%d.png",imageNamePrefix,(int)xx,(int)yy];
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void) newLocation:(Pnt *)pnt accuracy:(double)accuracy {
  //NSLog(@"Location update (%f,%f)",lon,lat);
  if(navigating) {
    NSInteger n=[locations count];
    // NSLog(@"Location update (%f,%f)  %d",pnt.lon,pnt.lat,(int)n);
    if(n>=500) {
      for(int i=0; i<n-300; i++) {
        [locations removeObjectAtIndex:0];
      }
    }
    [self addLocation:pnt];
  }
  else {
    _spin.hidden=YES;
    [_spin stopAnimating];
    _actionBtn.enabled=YES;
    Level *l0=(Level *)[appData.levels objectAtIndex:0];
    NSInteger sz=l0.tileSize/2;
    
    if([GeoUtil sectionInRange:l0.zoom mid:GeoPointMake(appData.lon,appData.lat) inc:GeoPointMake(pnt.lon,pnt.lat) xSpan:sz ySpan:sz]) {
      navigating = TRUE;
      UIImage *mv = [UIImage imageNamed:@"Pause0.png"];
      _actionBtn.imageView.image = mv;
      [self addLocation:pnt];
    }
    else {
      [service stop];
      double d = [GeoUtil distanceBetweenA:GeoPointMake(appData.lon, appData.lat) andB:GeoPointMake(pnt.lon, pnt.lat)];
      [self showDialog:3 message:[NSString stringWithFormat:@"Out of range: %d",(int)d]];
    }
  }
  
}

- (void) setTarget:(Place *)newTarget {
  target = newTarget;
  Level *z = [appData getCurrent];
  GeoPoint mid;
  if(target==nil) {
    mid=[self midPoint];
    [self createMapTile:TRUE x:xWsg y:yWsg];
    _mapPanel.image = current;
    [self relocate];
  }
  else {
    mid = GeoPointMake(target.lon, target.lat);
    if(navigating && [locations count]>0) {
      Pnt *p=[locations lastObject];
      Level *newLevel = [GeoUtil pickLevel:appData mid:mid inc:GeoPointMake(p.lon, p.lat) xSpan:deviceWidth/2 ySpan:deviceHeight/2];
      Level *z = [appData getCurrent];
      while(z.level>newLevel.level) {
        z = appData.goPrevious;
      }
      currentZoom = newLevel.zoom;
    }
  }
  [self doRender:z lon:mid.lon lat:mid.lat];
}

-(void) addLocation:(Pnt *)pnt; {
  GeoPoint gp =[self midPoint];
  Level *newLevel = [GeoUtil pickLevel:appData mid:gp inc:GeoPointMake(pnt.lon,pnt.lat) xSpan:deviceWidth/2 ySpan:deviceHeight/2];
  Level *z = [appData getCurrent];
  while(z.level>newLevel.level) {
    z = appData.goPrevious;
  }
  currentZoom = newLevel.zoom;
  [locations addObject:pnt];
  [self doRender:z lon:gp.lon lat:gp.lat];
}

- (void) acquireFailed {
  if(navigating) {
    [self showDialog:3 message:@"Location Service stops to work."];
    [locations removeAllObjects];
    navigating = FALSE;
    UIImage *mv = [UIImage imageNamed:@"Resume0.png"];
    _actionBtn.imageView.image=mv;
  }
  else {
    _spin.hidden=YES;
    [_spin stopAnimating];
    _actionBtn.enabled=YES;
    [self showDialog:3 message:@"Fail to retrieve satilite"];
  }
}

- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
  return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate {
  return NO;
}

- (NSUInteger)supportedInterfaceOrientations {
  return UIInterfaceOrientationMaskPortrait;
}

@end
