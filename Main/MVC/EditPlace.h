//
//  EditPlace.h
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MapController;
@class AppData;
@class Place;
@class NSManagedObject;

@interface EditPlace : UIViewController <UITextFieldDelegate,UIPickerViewDelegate, UIPickerViewDataSource>{
  __weak IBOutlet UITextField *nameInput;
  __weak IBOutlet UITextField *secondName;
  __weak IBOutlet UITextField *longitudeInput;
  __weak IBOutlet UITextField *latitudeInput;
  __weak IBOutlet UIBarButtonItem *deleteBtn;
  __weak IBOutlet UIPickerView *pathPicker;
  __weak IBOutlet UISwitch *enableCheck;
  __weak IBOutlet UITextField *indexInput;
  __weak IBOutlet UILabel *enableCheckLabel;
  MapController *main;
  AppData *appData;
  Place *spot;
  NSArray *docList;
  NSArray *imageList;
}

- (void) initialCommit;
- (NSManagedObject *) findByLon:(double) lon andLat:(double)lat;
-(void) populateManagedObject:(NSManagedObject *)obj withSpot:(Place *)spot;

- (IBAction)doCancel:(id)sender;
- (IBAction)doSave:(id)sender;
- (IBAction)doDelete:(id)sender;

- (void) editPlace:(MapController *)main appData:(AppData *)appData place:(Place*) spot;
- (NSString *) validateInput;

- (void) showDialog:(NSString *)msg;

@end
