//
//  ViewPlace.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "ViewPlace.h"
#import "EditPlace.h"
#import "AppData.h"
#import "Place.h"
#import "Path.h"
#import "GeoUtil.h"

@interface ViewPlace ()

@end

@implementation ViewPlace

- (void)viewDidLoad {
    [super viewDidLoad];
  Path * path;
  if (spot==nil) {
     titleItem.rightBarButtonItem = nil;
     titleItem.title = appData.city;
     path=[[Path alloc] initWithName:@"About.pdf" customized:FALSE];
  }
  else {
    path=spot.docPath;
    titleItem.title=spot.name;
  }
  NSData *data=[GeoUtil fetchData:path];

  [webView loadData:data MIMEType: @"application/pdf" textEncodingName: @"UTF-8" baseURL:nil];
  /*
  
  
  if(!p.customized) {
   NSString *docPath=[p.name componentsSeparatedByString:@"."][0];
  NSString *path = [[NSBundle mainBundle] pathForResource:docPath ofType:@"pdf"];
  NSURL *url = [NSURL fileURLWithPath:path];
  NSURLRequest *request = [NSURLRequest requestWithURL:url];
  [webView loadRequest:request];
}*/
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewPlace:(MapController *)ctrl appData:(AppData *)data place:(Place*)place {
  main = ctrl;
  appData = data;
  spot = place;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)toListView:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doEdit:(id)sender {
  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  EditPlace *placeManager=(EditPlace *)[mainStoryboard instantiateViewControllerWithIdentifier:@"EditPlace"];
  [placeManager editPlace:main appData:appData place:spot];
  [self.navigationController pushViewController:placeManager animated:YES];
}
@end
