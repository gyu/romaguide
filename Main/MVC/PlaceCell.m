//
//  SpotTableViewCell.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-12.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "PlaceCell.h"

@implementation PlaceCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
