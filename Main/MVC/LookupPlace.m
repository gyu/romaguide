//
//  LookupPlace.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "LookupPlace.h"

#import "AppData.h"
#import "Place.h"
#import "LookupCell.h"
#import "MapController.h"
#import "GeoUtil.h"

@interface LookupPlace ()

@end

@implementation LookupPlace

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [places count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  static NSString *CellIdentifier = @"LookupCell";
  LookupCell *cell = (LookupCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  Place *spot=(Place *)[places objectAtIndex:indexPath.row];
  cell.nameLabel.text=spot.name;
  cell.distanceLabel.text= [GeoUtil distanceStr:spot.distance ];
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  Place *spot=(Place *)[places objectAtIndex:indexPath.row];
  [main setTarget:spot];
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)toMapView:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doCancel:(id)sender {
  [main setTarget:nil ];
  [self.navigationController popViewControllerAnimated:YES];

}

- (void) lookupPlace:(MapController *)ctrl data:(AppData *)data {
  main=ctrl;
  GeoPoint p=[main refPoint];
  for(Place *spot in data.places) {
    spot.distance=[GeoUtil distanceBetweenA:p andB:GeoPointMake(spot.lon, spot.lat) ];
  }
  places=[data.places sortedArrayUsingSelector:@selector(distanceCompare:)];
}
@end
