//
//  LookupPlace.h
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AppData;
@class MapController;

@interface LookupPlace : UIViewController<UITableViewDelegate, UITableViewDataSource> {
   //AppData *appData;
   MapController *main;
   NSArray *places;
}
- (IBAction)toMapView:(id)sender;
- (IBAction)doCancel:(id)sender;

- (void) lookupPlace:(MapController *)main data:(AppData *)data;
@end
