//
//  MapController.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-05.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GeoUtil.h"
@class MapView;
@class AppData;
@class Place;
@class Level;
@class Pnt;
@class GeoService;

@interface MapController : UIViewController {
  GeoService *service;
  UIImage *current;
  NSInteger xWsg;
  NSInteger yWsg;
  NSInteger xTileCenter;
  NSInteger yTileCenter;
  NSInteger deviceWidth;
  NSInteger deviceHeight;
  NSString *imageNamePrefix;
  AppData *appData;
  int tileNumber;
  int tileSize;
  int shiftHorizon;
  int shiftVertical;
  int deltaHorizonl;
  int deltaVertical;
  NSInteger currentZoom;
  double dlon;
  double dlat;
  NSInteger middleX;
  NSInteger middleY;
  BOOL navigating;
  NSMutableArray * locations;
  Place *target;
}
@property (weak, nonatomic) IBOutlet UIButton *zoomDown;
@property (weak, nonatomic) IBOutlet UIButton *zoomUp;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;
@property (weak, nonatomic) IBOutlet UIImageView *mapPanel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spin;

- (IBAction)startNavagation:(id)sender;
- (IBAction)managePlaces:(id)sender;
- (IBAction)lookupPlace:(id)sender;
- (IBAction)zoomIn:(id)sender;
- (IBAction)zoomOut:(id)sender;
- (BOOL)createMapTile:(BOOL)create x:(NSInteger)x y:(NSInteger)y;
- (BOOL)pickColumn:(BOOL)create column:(NSInteger) coloumn y: (NSInteger)y;
- (BOOL)pickRow:(BOOL)create row:(NSInteger)row x: (NSInteger)x;
- (BOOL)pickCell:(BOOL)create row:(NSInteger)row column: (NSInteger)column;
- (void) initialize;
- (void) relocate;
- (UIImage *) subImage:(CGRect)rect row:(NSInteger)row column:(NSInteger)column;
- (NSString *) imageNameTileX:(NSInteger)x TileY:(NSInteger)y;
- (void) changeZoom:(NSInteger)delta;
- (void) drawOverlay;
- (GeoPoint) refPoint;
- (GeoPoint) midPoint;
- (void) spotSelected:(Place*) spot;
- (void) selectedLon:(double) lon lat:(double)lat;
- (void) newLocation:(Pnt *)pnt accuracy:(double)accuracy;

- (void) acquireFailed;
- (void) showDialog:(NSInteger) duration message:(NSString*) message;
- (void) addLocation:(Pnt *)pnt;
- (void) doRender:(Level *)lvl lon:(double)lon lat:(double)lat;

- (void) setTarget:(Place *)target;
- (void) reloadAppData;

- (void) drawTestRoute:(CGContextRef)context;

@end
