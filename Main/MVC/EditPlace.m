//
//  EditPlace.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "EditPlace.h"
#import "MapController.h"
#import "AppData.h"
#import "Place.h"
#import "Path.h"
#import "Level.h"
#import "AppDelegate.h"
#import "JsonParser.h"
#import "GeoUtil.h"

@interface EditPlace ()

@end

@implementation EditPlace

- (void)viewDidLoad {
  [super viewDidLoad];
  docList = [GeoUtil pdfList];
  imageList = [GeoUtil imageList];
  if(spot == nil || spot.original) {
    deleteBtn.style = UIBarButtonItemStylePlain;
    deleteBtn.enabled = false;
    deleteBtn.title = nil;
  }
  if(spot == nil || !spot.original) {
    enableCheck.hidden=YES;
    enableCheckLabel.hidden=YES;
  }
  if(spot != nil)
  {
    nameInput.text =spot.name;
    secondName.text = spot.nativeName;
    longitudeInput.text = [[NSNumber numberWithDouble:spot.lon] stringValue];
    latitudeInput.text = [[NSNumber numberWithDouble:spot.lat] stringValue];
    indexInput.text = [[NSNumber numberWithFloat:spot.index] stringValue];
    if(spot.original) {
      longitudeInput.enabled = FALSE;
      latitudeInput.enabled = FALSE;
    }
    [pathPicker reloadAllComponents];
    if(spot.docPath.customized && [docList containsObject:spot.docPath.name]) {
      [pathPicker selectRow:([docList indexOfObject:spot.docPath.name]+1) inComponent:0 animated:NO];
    }
    if(spot.logoPath.customized && [imageList containsObject:spot.logoPath.name]) {
      [pathPicker selectRow:([imageList indexOfObject:spot.logoPath.name]+1) inComponent:1 animated:NO];
    }
  }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) populateManagedObject:(NSManagedObject *)obj withSpot:(Place *)spt {
  [obj setValue:spt.name forKey:@"name"];
  [obj setValue:spt.nativeName forKey:@"nativeName"];
  [obj setValue:spt.info forKey:@"info"];
  [obj setValue:spt.docPath.name forKey:@"docPath"];
  [obj setValue:spt.logoPath.name forKey:@"logoPath"];
  [obj setValue:[NSNumber numberWithBool:spt.original] forKey:@"original"];
  [obj setValue:[NSNumber numberWithBool:spt.docPath.customized] forKey:@"docCustomized"];
  [obj setValue:[NSNumber numberWithBool:spt.logoPath.customized] forKey:@"logoCustomized"];
  [obj setValue:[NSNumber numberWithDouble:spt.lon] forKey:@"lon"];
  [obj setValue:[NSNumber numberWithDouble:spt.lat] forKey:@"lat"];
  [obj setValue:[NSNumber numberWithFloat:spt.index] forKey:@"index"];
}
- (void) initialCommit {
  Place *first = [appData.places firstObject];
  if(first.objId != nil) {
    return;
  }
  AppDelegate *app = [[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = app.managedObjectContext;
  NSEntityDescription *desc = [NSEntityDescription entityForName:@"Place" inManagedObjectContext:context];
  for(Place *spt in appData.places)  {
    NSManagedObject *obj = [[NSManagedObject alloc] initWithEntity:desc insertIntoManagedObjectContext:context];
    [self populateManagedObject:obj withSpot:spt];
  }
  [app saveContext];
}

- (NSManagedObject *) findByLon:(double) lon andLat:(double)lat {
  AppDelegate *app=[[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = app.managedObjectContext;
  NSEntityDescription *desc=[NSEntityDescription entityForName:@"Place" inManagedObjectContext:context];
  NSFetchRequest *request= [[NSFetchRequest alloc] init];
  [request setEntity:desc];
  NSPredicate *predicate=[NSPredicate predicateWithFormat:@"lon=%f and lat=%f",lon,lat];
  [request setPredicate: predicate];
  NSError *err=nil;
  NSArray *res=[context executeFetchRequest:request error:&err];
  if(err!=nil||res==nil||[res count]==0) return nil;
  return [res firstObject];
}

- (IBAction)doCancel:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doSave:(id)sender {
  NSString *errMsg = [self validateInput];
  if (errMsg!=nil) {
    [self showDialog:errMsg];
    return;
  }
  [self initialCommit];
  AppDelegate *app = [[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = app.managedObjectContext;
  NSEntityDescription *desc = [NSEntityDescription entityForName:@"Place" inManagedObjectContext:context];
  NSManagedObject *obj;
  Place *place;
  NSError *err;
  if(spot == nil) {
    place = [[Place alloc] init];
    place.original = FALSE;
    obj = [[NSManagedObject alloc] initWithEntity:desc insertIntoManagedObjectContext:context];
  }
  else {
    place = spot;
    if(spot.objId==nil) {
      obj = [self findByLon:spot.lon andLat:spot.lat];
    }
    else {
      obj = [context existingObjectWithID:spot.objId error:&err];
    }
  }
  place.name=nameInput.text;
  place.nativeName=secondName.text;
  place.lon=[longitudeInput.text doubleValue];
  place.lat=[latitudeInput.text doubleValue];
  place.index=[indexInput.text floatValue];
  NSInteger sel = [pathPicker selectedRowInComponent:0];
  if(sel==0) {
    if(spot!=nil) {
      if(place.docPath.customized) {
        place.docPath.customized=FALSE;
        place.docPath.name=[JsonParser findDocPathAtLon:place.lon andLat:place.lat];
      }
    }
  }
  else {
    NSString *fileName=[docList objectAtIndex:sel-1];
    if(spot!=nil) {
      place.docPath.name=fileName;
      place.docPath.customized=TRUE;
    }
      else {
      place.docPath=[[Path alloc] initWithName:fileName customized:TRUE];
  
     }
  }
  sel = [pathPicker selectedRowInComponent:1];
  if(sel==0) {
    if(spot!=nil) {
      if(place.logoPath.customized) {
        place.logoPath.customized=FALSE;
        place.logoPath.name=[JsonParser findLogoPathAtLon:place.lon andLat:place.lat];
      }
    }
  }
  else {
    NSString *fileName=[imageList objectAtIndex:sel-1];
    if(spot != nil) {
      place.logoPath.name=fileName;
      place.logoPath.customized=TRUE;
    }
    else {
      place.logoPath=[[Path alloc] initWithName:fileName customized:TRUE];
    }
  }
  [self populateManagedObject:obj withSpot:place];
  [app saveContext];
  [main reloadAppData];
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doDelete:(id)sender {
  NSError *err;
  AppDelegate *app=[[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context = app.managedObjectContext;
  NSManagedObject *obj = [context existingObjectWithID:spot.objId error:&err];
  [context deleteObject:obj];
  [app saveContext];
  [main reloadAppData];
  [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
  return 2;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  if(row==0) return @"Default";
  NSArray *list = (component==0)? docList:imageList;
  return [list objectAtIndex:row-1];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
  NSInteger cnt = 1 + (component==0? [docList count]:[imageList count]);
    return cnt;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  return YES;
}

- (void) editPlace:(MapController *)ctrl appData:(AppData *)data place:(Place*)place {
  main = ctrl;
  appData = data;
  spot = place;
}

- (NSString *) validateInput {
  NSString *msg = nil;
  NSString * str = nameInput.text;
  if ([str length] == 0) {
    msg=@"Name is required.";
  }
  else {
    str = indexInput.text;
    if ([str length] == 0) {
      msg=@"Popularity index is required.";
    }
    else {
      float findex = str.floatValue;
      if (findex < 0.01 || findex > 1.0 ) {
        msg=@"In valid popularity index";
      }
      else {
        str = longitudeInput.text;
         if ([str length] == 0) {
        msg=@"Longitude is required.";
      }
      else {
        double lon=str.doubleValue;
        
        str =latitudeInput.text;
        if ([str length] == 0) {
          msg=@"Latitude is required.";
        }
        else {
          double lat=str.doubleValue;
          Level *l0=(Level *)[appData.levels objectAtIndex:0];
          NSInteger sz=l0.tileSize/2;
          
          if(![GeoUtil sectionInRange:l0.zoom mid:GeoPointMake(appData.lon,appData.lat) inc:GeoPointMake(lon,lat) xSpan:sz ySpan:sz]) {
            msg=@"Position for the place is out-of-range.";
          }
        }
      }
      }
    }
  }
   return msg;
}

- (void) showDialog:(NSString *)msg {
  int duration = 3;
  UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                  message:msg
                                                 delegate:nil
                                        cancelButtonTitle:nil
                                        otherButtonTitles:nil, nil];
  [toast show];
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
    [toast dismissWithClickedButtonIndex:0 animated:YES];
  });

}
@end
