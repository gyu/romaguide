//
//  ViewPlace.h
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MapController;
@class AppData;
@class Place;

@interface ViewPlace : UIViewController {
  MapController *main;
  AppData *appData;
  Place *spot;
  __weak IBOutlet UIWebView *webView;
  __weak IBOutlet UINavigationItem *titleItem;
}

- (void) viewPlace:(MapController *)main appData:(AppData *)appData place:(Place*) spot;

- (IBAction)toListView:(id)sender;
- (IBAction)doEdit:(id)sender;

@end
