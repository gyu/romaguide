//
//  SpotManager.h
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MapController;
@class AppData;

@interface PlaceListView : UIViewController<UITableViewDelegate, UITableViewDataSource> {
  MapController *main;
  AppData *appData;
  __weak IBOutlet UINavigationBar *navBar;
  __weak IBOutlet UINavigationItem *navItem;
}

- (IBAction)toMapView:(id)sender;
- (IBAction)newPlace:(id)sender;

- (void) managePlaces:(MapController *)main appData:(AppData *)appData;

- (void) navTitleSelected :(UITapGestureRecognizer *)recognizer;

@end
