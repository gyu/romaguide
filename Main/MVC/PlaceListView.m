//
//  SpotManager.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "PlaceListView.h"
#import "GeoUtil.h"
#import "AppData.h"
#import "Place.h"
#import "PlaceCell.h"
#import "EditPlace.h"
#import "ViewPlace.h"

@interface PlaceListView ()

@end

@implementation PlaceListView

- (void)viewDidLoad {
    [super viewDidLoad];
  /*UILabel *label=[[UILabel alloc] init];
  label.frame=CGRectMake(2, 2, 200, 30);
  label.backgroundColor=[UIColor clearColor];
  label.textColor=[UIColor blackColor];
  label.font=[UIFont systemFontOfSize:14];*/
  navItem.title=appData.city;
  UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(navTitleSelected:)];
  for(UIView *v in navBar.subviews) {
    if ([v isKindOfClass:NSClassFromString(@"UINavigationItemView")]
          &&![v isKindOfClass:NSClassFromString(@"UINavigationItemButtonView")]) {
      [v setUserInteractionEnabled:YES];
      [v addGestureRecognizer:tap];
    }
  }
    // Do any additional setup after loading the view.
}

- (void) navTitleSelected :(UITapGestureRecognizer *)recognizer{
  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  ViewPlace *placeManager=(ViewPlace *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ViewPlace"];
  [placeManager viewPlace:main appData:appData place:nil];
  [self.navigationController pushViewController:placeManager animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [appData.places count];
}

- (void) managePlaces:(MapController *)ctrl appData:(AppData *)data {
  main=ctrl;
  appData=data;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  static NSString *CellIdentifier = @"spotTableCell";
  PlaceCell *cell = (PlaceCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  Place *spot=(Place *)[appData.places objectAtIndex:indexPath.row];
  cell.spotImage.image=[GeoUtil imageForPath:spot.logoPath];
  cell.spotLable.text=spot.name;
  cell.nameLabel.text=spot.nativeName;
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  ViewPlace *placeManager=(ViewPlace *)[mainStoryboard instantiateViewControllerWithIdentifier:@"ViewPlace"];
  Place *spot=(Place *)[appData.places objectAtIndex:indexPath.row];
  [placeManager viewPlace:main appData:appData place:spot];
  [self.navigationController pushViewController:placeManager animated:YES];
}

- (IBAction)toMapView:(id)sender {
  [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)newPlace:(id)sender {
  UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
  EditPlace *placeManager=(EditPlace *)[mainStoryboard instantiateViewControllerWithIdentifier:@"EditPlace"];
  [placeManager editPlace:main appData:appData place:nil];
  [self.navigationController pushViewController:placeManager animated:YES];
 
}
@end
