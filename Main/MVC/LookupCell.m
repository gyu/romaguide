//
//  LookupCell.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-14.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "LookupCell.h"

@implementation LookupCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
