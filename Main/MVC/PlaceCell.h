//
//  SpotTableViewCell.h
//  Waterloo
//
//  Created by Ge Yu on 2015-02-12.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *spotImage;
@property (weak, nonatomic) IBOutlet UILabel *spotLable;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end
