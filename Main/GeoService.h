//
//  GeoService.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-08.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "GeoUtil.h"

@class MapController;
@class Pnt;

@interface GeoService : NSObject<CLLocationManagerDelegate> {
  MapController *app;
  BOOL updating;
  BOOL started;
  BOOL updateErr;
  Pnt *lastSent;
  NSMutableArray * locations;
  NSTimer *failCheck;
  CLLocationManager *manager;
  double accuracy;
}

- (id)initWithMain:(MapController *) app;
- (void) start;
- (void) stop;
- (void) doFail;
- (void) doSubmit;
- (GeoPoint) makeBest;

@end