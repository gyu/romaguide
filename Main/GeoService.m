//
//  GeoService.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-08.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "GeoService.h"
#import "MapController.h"
#import "Pnt.h"

@implementation GeoService

- (id)initWithMain:(MapController *) ainst
{
  self = [super init];
  if (self) {
    app=ainst;
    updating = FALSE;
    updateErr = TRUE;
    started = FALSE;
    locations = [[NSMutableArray alloc] init];
  }
  return self;
}


- (void) start {
  manager = [[CLLocationManager alloc] init];
  manager.desiredAccuracy = kCLLocationAccuracyBest;
  manager.delegate = self;
  if ([manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    [manager requestWhenInUseAuthorization];
  }
  started = FALSE;
  updateErr = FALSE;
  lastSent=nil;
  accuracy=0;
  [locations  removeAllObjects];
  [manager startUpdatingLocation];
  updating=TRUE;
}

- (void) stop {
   updating=FALSE;
   started = FALSE;
   updateErr = FALSE;
  lastSent=nil;
  [locations  removeAllObjects];
   [manager stopUpdatingLocation];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager
   didUpdateToLocation:(CLLocation *)newLocation
          fromLocation:(CLLocation *)oldLocation
{
  if (updateErr) {
    updateErr = FALSE;
    if(failCheck!=nil) {
      [failCheck invalidate];
      failCheck = nil;
    }
    
  }
  
  if(!updating) {
    NSLog(@"Updating doesn't stop");
    [self stop];
  }
  else {
    NSDate *now = [NSDate date];
    Pnt *pnt = [[Pnt alloc] initWithDate:now lon:newLocation.coordinate.longitude lat:newLocation.coordinate.latitude];
    accuracy += newLocation.horizontalAccuracy;
    if(!started) {
      if ( [locations count]==0 ) {
        [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(waitStart:) userInfo:nil repeats:NO];
      }
      [locations addObject:pnt];
    }
    else {
      [locations addObject:pnt];
      NSTimeInterval val = [now timeIntervalSinceDate:lastSent.date];
      if(val<5) return;
      GeoPoint p= [self makeBest];
      if([GeoUtil distanceBetweenA:p andB:GeoPointMake(lastSent.lon, lastSent.lat)]<10) {
          if(val<10) return;
      }
      lastSent = [[Pnt alloc] initWithDate:now lon:p.lon lat:p.lat];
      [self doSubmit];
    }
  }
}

- (void)waitStart:(NSTimer *)aTimer{
  started = TRUE;
  GeoPoint p= [self makeBest];
  lastSent = [[Pnt alloc] initWithDate:[NSDate date] lon:p.lon lat:p.lat];
  [self doSubmit];
}

- (void) doSubmit {
  NSInteger n = [locations count];
  [app newLocation:lastSent accuracy: accuracy/n];
  [locations removeAllObjects];
  accuracy = 0;
}

- (void)checkFailed:(NSTimer *)aTimer{
  failCheck = nil;
  [self doFail];
}

- (void) doFail {
  [self stop];
  [app acquireFailed];
  started=FALSE;
  updateErr = FALSE;
  if(failCheck !=nil) {
    [failCheck invalidate];
    failCheck = nil;
  }
}

- (GeoPoint) makeBest {
  
  NSInteger n=[locations count];
  //NSLog(@"Make Best %d",(int)n);
  double x=0;
  double y=0;
  double x2=0;
  double y2=0;
  for(int i=0; i<n; i++) {
      Pnt *p = [locations objectAtIndex:i];
      x += p.lon;
      y += p.lat;
      x2 += p.lon*p.lon;
      y2 += p.lat*p.lat;
  }
  double nx = x/n;
  double ny = y/n;
  double nx2 = x2/n;
  double ny2 = y2/n;
  double sx = nx2-nx*nx;
  double sy = ny2-ny*ny;
  double lon = nx;
  double lat = ny;
  if(n>2) {
    for(int i=1; i<5; i++) {
      double dx = sx*i;
      double dy = sy*i;
      int m = 0;
      x = 0;
      y = 0;
      for(int j=0; j<n; j++) {
        Pnt *p = [locations objectAtIndex:j];
        if(fabs(p.lon-nx)<dx && fabs(p.lat-ny)<dy) {
          m++;
          x += p.lon;
          y += p.lat;
        }
      }
      if( m>0 ) {
        lon = x/m;
        lat = y/m;
        break;
      }
    }
    
  }
  return GeoPointMake(lon, lat);
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
  if (!updateErr) {
     updateErr = TRUE;
     failCheck = [NSTimer scheduledTimerWithTimeInterval:3 target:self
        selector:@selector(checkFailed:) userInfo:nil repeats:NO];
  }
  else {
     [self doFail];
  }

}


@end
