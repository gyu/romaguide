//
//  Path.h
//  Waterloo
//
//  Created by Ge Yu on 2015-02-17.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Path : NSObject

@property NSString *name;
@property BOOL customized;

- (id)initWithName:(NSString*)name customized:(BOOL)customized;

@end
