//
//  GeoUtil.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-08.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "GeoUtil.h"
#import "Level.h"
#import "AppData.h"
#import "Path.h"

@implementation GeoUtil

double const MaxLat=85.051128;

+ (WsgPoint) toWsgWithZoom:(NSInteger)zoom longitude:(double)lon latitude:(double)lat {
    int shift = zoom + 7;
		double x = lon / 180.0 + 1.0;
		int cx = (int) (x  * (1 << shift));
		if (lat > MaxLat)
      lat = MaxLat;
		if (lat < -MaxLat)
      lat = -MaxLat;
   	double sinf = sin(-lat*M_PI/180.0);
 		double y = log((1.0 + sinf) / (1.0 - sinf)) / 6.2831853071795862 + 1.0;
 		int cy = (int) (y * (1 << shift));
		int bit = 1 << (zoom + 9);
		int tmpX = cx & (bit - 1);
    return WsgPointMake(tmpX, cy);
}

+ (GeoPoint) toGeoWithZoom:(NSInteger)zoom xcoord:(NSInteger)x ycoord:(NSInteger)y {
  int shift = zoom + 7;
		int mapSize = 1 << (zoom + 9);
		int tmpX = x & (mapSize - 1);
		double tx = (double) tmpX / (double) (1 << shift) - 1.0;
		double lon = tx * 180.0;
		double ty = (double) y / (double) (1 << shift) - 1.0;
		ty *= 3.1415926535897931;
		double ey = atan(exp(ty))*180.0/M_PI;
		double lat = ey * 2 - 90;
    return GeoPointMake(lon, -lat);
}

+ (double) distanceBetweenA:(GeoPoint)a andB:(GeoPoint)b {
  double p1latr =a.lat*M_PI/180.0;
		double p2latr = b.lat*M_PI/180.0;
		double dlat = (p2latr - p1latr) / 2.0;
		double dlon = (b.lon - a.lon)* M_PI /360.0;
		double s1 = sin(dlat);
		double s2 = sin(dlon);
		double h = s1 * s1 + cos(p1latr) * cos(p2latr) * s2 * s2;
		double dist = 12734908 * asin(sqrt(h));
		return dist;
}

+ (NSString *) distanceStrBetweenA:(GeoPoint)a andB:(GeoPoint)b {
  return [GeoUtil distanceStr:[ GeoUtil distanceBetweenA:a andB:b]];
}

+ (NSString *) distanceStr:(double) d {
  if(d>100000) return [NSString stringWithFormat:@"%d km",(int)(d*0.001)];
  if(d>=2000) return [NSString stringWithFormat:@"%.2f km",d*0.001];
  return [NSString stringWithFormat:@"%.0f m",d];
}

+ (double) unitDistanceAt:(WsgPoint)pnt zoom:(NSInteger)zoom {
    int len=50;
		int shift = zoom + 7;
		int mapSize = 1 << (zoom + 9);
		double ty = (double) pnt.yPos / (double) (1 << shift) - 1.0;
		ty *= 3.1415926535897931;
		double ey = atan(exp(ty));
		double lat = ey * 2 - 0.5*M_PI;
		double tx1 = 0.5*((pnt.xPos-len) & mapSize - 1) / (1 << shift) - 1;
		double tx2 = 0.5*((pnt.xPos+len) & mapSize - 1) / (1 << shift) - 1;
		double dlon = (tx2-tx1)*M_PI;
		double s2 = sin(dlon);
		double s1 = cos(lat);
		double h = s1 * s1 + s2 * s2;
		return 12734908 * asin(sqrt(h))/(len+len);
}

+ (BOOL) sectionInRange:(NSInteger)zoom mid:(GeoPoint)mid inc:(GeoPoint)inc xSpan:(NSInteger)x ySpan:(NSInteger)y {
  WsgPoint center = [GeoUtil toWsgWithZoom:zoom longitude:mid.lon latitude:mid.lat];
  WsgPoint pnt = [GeoUtil toWsgWithZoom:zoom longitude:inc.lon latitude:inc.lat];
  if(abs(pnt.xPos-center.xPos)>x||abs(pnt.yPos-center.yPos)>y) {
    return FALSE;
  }
  return TRUE;
}

+ (Level *) pickLevel:(AppData *)data mid:(GeoPoint)mid inc:(GeoPoint)inc xSpan:(NSInteger)x ySpan:(NSInteger)y{
  NSArray *levels=data.levels;
  NSInteger n=[levels count];
  NSInteger current = [data getCurrent].zoom;
  for(int i=n-1; i>0; i--) {
    Level *lvl=(Level *)[levels objectAtIndex:i];
    if(lvl.zoom <= current) {
      if([GeoUtil sectionInRange:lvl.zoom mid:mid inc:inc xSpan:x ySpan:y]) {
        return lvl;
      }
    }
  }
  return (Level *)[levels objectAtIndex:0];
}

+ (BOOL) isPdf:(NSString *)ext {
  return ( [ext caseInsensitiveCompare:@"pdf"] == NSOrderedSame );
}

+ (BOOL) isImage:(NSString *)ext {
  if ( [ext caseInsensitiveCompare:@"png"] == NSOrderedSame ) return TRUE;
  if ( [ext caseInsensitiveCompare:@"gif"] == NSOrderedSame ) return TRUE;
  if ( [ext caseInsensitiveCompare:@"jpg"] == NSOrderedSame ) return TRUE;
  if ( [ext caseInsensitiveCompare:@"jpeg"] == NSOrderedSame ) return TRUE;
  return FALSE;
}

+ (NSArray *) fileList {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  
  NSFileManager *manager = [NSFileManager defaultManager];
  return [manager contentsOfDirectoryAtPath:documentsDirectory error:nil];
}

+ (NSArray *) pdfList {
  NSArray *files = [GeoUtil fileList];
  NSMutableArray *res = [[NSMutableArray alloc] init];
  for(id item in files )
  {
    NSString *path = (NSString *)item;
    if( [GeoUtil isPdf:[path pathExtension]] ) {
      [res addObject:path];
    }
    
  }

  return res;
}

+ (NSArray *) imageList {
  NSArray *files=[GeoUtil fileList];
  NSMutableArray *res= [[NSMutableArray alloc] init];
  for(id item in files )
  {
    NSString *path=(NSString *)item;
    if( [GeoUtil isImage:[path pathExtension]] ) {
      [res addObject:path];
    }
    
  }
  return res;
}

+ (NSData *)loadData:(NSString *) file {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  NSString *filePath = [documentsDirectory stringByAppendingPathComponent:file];
  NSURL *fileUrl = [NSURL fileURLWithPath:filePath];
  return[NSData dataWithContentsOfURL:fileUrl];
}

+ (NSData *) fetchData:(Path *)path {
  if(path.customized) {
    return [GeoUtil loadData:path.name];
  }
  else {
     NSString *docName=[path.name componentsSeparatedByString:@"."][0];
     NSString *bundlePath = [[NSBundle mainBundle] pathForResource:docName ofType:@"pdf"];
     return [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:bundlePath]];
  }
  
}

+ (UIImage *) imageForPath:(Path *)path {
  if( !path.customized ) {
    return [UIImage imageNamed:path.name];
  }
  return [UIImage imageWithData:[GeoUtil loadData:path.name]];
}

@end
