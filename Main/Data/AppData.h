//
//  Entity.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-07.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Level;
@class Place;


@interface AppData : NSObject {
   NSInteger spotCount;
   NSInteger levelCount;
  Level *currentLevel;
}
@property NSString *city;
@property NSString *region;
@property NSString *info;
@property double lon;
@property double lat;
@property NSArray *places;
@property NSArray *levels;
@property NSString *logoPath;

- (id)initWithCity:(NSString*)city region:(NSString*)region longitude:(double)lon latitude:(double)lat places:(NSArray *)spots levels:(NSArray *)levels info:(NSString*)info logoPath:(NSString*)logoPath;

- (BOOL) hasPreviousLevel;
- (BOOL) hasNextLevel;

- (Level *) goPrevious;
- (Level *) goNext;
- (Level *) getCurrent;

@end
