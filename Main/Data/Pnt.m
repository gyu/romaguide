//
//  Point.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-10.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "Pnt.h"

@implementation Pnt

- (id) initWithDate:(NSDate*)date lon:(double)lon lat:(double)lat {
  self = [super init];
  if (self) {
    self.date = date;
    self.lon = lon;
    self.lat = lat;
  }
  return self;
}

@end
