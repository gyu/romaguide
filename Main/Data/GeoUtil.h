//
//  GeoUtil.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-08.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CGGeometry.h>

struct WsgPoint {
  NSInteger xPos;
  NSInteger yPos;
};

typedef struct WsgPoint WsgPoint;

struct GeoPoint {
  double lon;
  double lat;
};

typedef struct GeoPoint GeoPoint;

CG_INLINE WsgPoint WsgPointMake(NSInteger x, NSInteger y) {
  WsgPoint point;
  point.xPos = x;
  point.yPos = y;
  return point;
}

CG_INLINE GeoPoint GeoPointMake(double x, double y) {
  GeoPoint point;
  point.lon = x;
  point.lat = y;
  return point;
}
extern double const MaxLat;
@class Path;
@class AppData;
@class Level;

@interface GeoUtil : NSObject
+ (WsgPoint) toWsgWithZoom:(NSInteger)zoom longitude:(double)lon latitude:(double)lat;
+ (GeoPoint) toGeoWithZoom:(NSInteger)zoom xcoord:(NSInteger)x ycoord:(NSInteger)y;
+ (double) distanceBetweenA:(GeoPoint)a andB:(GeoPoint)b;
+ (double) unitDistanceAt:(WsgPoint)pnt zoom:(NSInteger)zoom;
//+ (BOOL) inRange:(NSInteger)level data:(Entity *)data lon:(double)lon lat:(double)lat;
+ (NSString *) distanceStrBetweenA:(GeoPoint)a andB:(GeoPoint)b;
+ (NSString *) distanceStr:(double) distance;
+ (BOOL) sectionInRange:(NSInteger)zoom mid:(GeoPoint)mid inc:(GeoPoint)inc xSpan:(NSInteger)x ySpan:(NSInteger)y;
+ (Level *) pickLevel:(AppData *)data mid:(GeoPoint)mid inc:(GeoPoint)inc xSpan:(NSInteger)x ySpan:(NSInteger)y;
+ (NSArray *) fileList;
+ (NSArray *) pdfList;
+ (NSArray *) imageList;
+ (BOOL) isPdf:(NSString *)path;
+ (BOOL) isImage:(NSString *)path;
+ (NSData *)loadData:(NSString *) file;
+ (NSData *) fetchData:(Path *)path;
+ (UIImage *) imageForPath:(Path *)path;


@end
