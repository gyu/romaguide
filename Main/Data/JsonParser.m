//
//  JsonParser.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-08.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "AppDelegate.h"
#import "JsonParser.h"
#import "AppData.h"
#import "Place.h"
#import "Path.h"
#import "Level.h"

@implementation JsonParser

+ (NSDictionary *) jsonData {
  NSString *filePath = [[NSBundle mainBundle] pathForResource: @"appData" ofType:@"json"];
  NSData *data = [[NSFileManager defaultManager] contentsAtPath:filePath];
  NSError *jsonParsingError = nil;
  
  NSMutableDictionary  *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonParsingError];
  if (jsonParsingError != nil) {
    NSLog(@"%@", [jsonParsingError localizedDescription]);
  }
  return json;
}


+(AppData *) readAppData {
  NSDictionary  *json = [JsonParser jsonData];
  NSArray *levelArry=[json objectForKey:@"levels"];
  
  NSMutableArray *levels=[[NSMutableArray alloc] init];
  NSMutableArray *spots=[[NSMutableArray alloc] init];
  
  for (id levelItem in levelArry) {
    NSDictionary  *levelDict = (NSDictionary  *)levelItem;
    NSInteger zoom =  [ [levelDict objectForKey:@"zoom"] intValue];
    NSInteger level =  [ [levelDict objectForKey:@"level"] intValue];
    NSInteger number =  [ [levelDict objectForKey:@"tileNumber"] intValue];
    NSInteger sz =  [ [levelDict objectForKey:@"tileSize"] intValue];
    NSString *prefix =  [levelDict objectForKey:@"pathPrefix"];
    NSLog(@"%d %d  %d %d  %@", level,zoom,number, sz,prefix);
    Level *item=[[Level alloc] initWithZoom:zoom level:level tileNumber:number tileSize:sz pathPrefix:prefix];
    [levels addObject:item];
    
  }
  NSArray *sortedLevels = [levels sortedArrayUsingSelector:@selector(compare:)];
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  NSManagedObjectContext *context=app.managedObjectContext;
  NSEntityDescription *appDataDesc=[ NSEntityDescription entityForName:@"Place" inManagedObjectContext:context];
  NSFetchRequest *request= [[NSFetchRequest alloc] init];
  [request setEntity:appDataDesc];
  NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"index" ascending:NO];
  NSSortDescriptor * nameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"nativeName" ascending:YES ];
  NSArray *sortDescriptors = @[sortDescriptor, nameDescriptor];
  [request setSortDescriptors:sortDescriptors];
  NSError *err = nil;
  NSArray *queryResult=[context executeFetchRequest:request error:&err];
  if( err!=nil || [queryResult count]==0) {
  NSArray *spotArry=[json objectForKey:@"spots"];
  for (NSDictionary  *spotDict in spotArry) {
    NSString *name=  [spotDict objectForKey:@"name"];
    NSString *name2=  [spotDict objectForKey:@"nativeName"];
    NSString *info =  [spotDict objectForKey:@"info"];
    NSString *doc =  [spotDict objectForKey:@"docPath"];
    NSString *logo =  [spotDict objectForKey:@"logoPath"];
    
    double lon =  [ [spotDict objectForKey:@"lon"] doubleValue];
    double lat =  [ [spotDict objectForKey:@"lat"] doubleValue];
    float index = [ [spotDict objectForKey:@"popularity"] floatValue];
    Place *item = [[Place alloc] initWithName:name nativeName:name2 longitude:lon latitude:lat doc:doc logo:logo index:index info:info ];
    [spots addObject:item];
  }
  }
  else {
    for(NSManagedObject *obj in queryResult) {
      Place *item = [[Place alloc] init];
      item.name = [obj valueForKey:@"name"];
      item.nativeName = [obj valueForKey:@"nativeName"];
      item.objId=[obj objectID];
      item.info = [obj valueForKey:@"info"];
      item.docPath = [[Path alloc] initWithName:[obj valueForKey:@"docPath"] customized:[JsonParser boolValue:obj key:@"docCustomized"]];
      item.logoPath = [[Path alloc] initWithName:[obj valueForKey:@"logoPath"] customized:[JsonParser boolValue:obj key:@"logoCustomized"]];
      item.lon=[JsonParser doubleValue:obj key:@"lon"];
      item.lat=[JsonParser doubleValue:obj key:@"lat"];
      item.index=[JsonParser floatValue:obj key:@"index"];
      item.original=[JsonParser boolValue:obj key:@"original"];
      [spots addObject:item];
    }
  }
  NSString *city=  [json objectForKey:@"city"];
  NSString *region=  [json objectForKey:@"region"];
  NSString *info =  [json objectForKey:@"info"];
  NSString *logo =  [json objectForKey:@"logoPath"];
  double lon =  [ [json objectForKey:@"lon"] doubleValue];
  double lat =  [ [json objectForKey:@"lat"] doubleValue];
  AppData *appData=[[AppData alloc] initWithCity:city region:region longitude:lon latitude:lat places:spots levels:sortedLevels info:info logoPath:logo];
  return appData;
}

+ (BOOL) boolValue:(NSManagedObject *)obj key:(NSString *) key {
  return [[obj valueForKey:key] boolValue];
}
+ (double) doubleValue:(NSManagedObject *)obj key:(NSString *) key {
  return [[obj valueForKey:key] doubleValue];
}
+ (float) floatValue:(NSManagedObject *)obj key:(NSString *) key {
  return [[obj valueForKey:key] floatValue];
}

+(NSString *) findDocPathAtLon:(double)lon andLat:(double)lat {
   NSDictionary  *json = [JsonParser jsonData];
  NSArray *spotArry=[json objectForKey:@"spots"];
  for (NSDictionary  *spotDict in spotArry) {
    double u=  [ [spotDict objectForKey:@"lon"] doubleValue];
    double v =  [ [spotDict objectForKey:@"lat"] doubleValue];
    if(u==lon && v==lat) {
      return [spotDict objectForKey:@"docPath"];
    }
   
  }
  return nil;
  
}

+(NSString *) findLogoPathAtLon:(double)lon andLat:(double)lat {
    NSDictionary  *json = [JsonParser jsonData];
    NSArray *spotArry=[json objectForKey:@"spots"];
    for (NSDictionary  *spotDict in spotArry) {
      double u=  [ [spotDict objectForKey:@"lon"] doubleValue];
      double v =  [ [spotDict objectForKey:@"lat"] doubleValue];
      if(u==lon && v==lat) {
        return [spotDict objectForKey:@"logoPath"];
      }
      
    }
    return nil;
}

@end

