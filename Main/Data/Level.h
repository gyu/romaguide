//
//  Level.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-07.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Level : NSObject
@property NSInteger level;
@property NSInteger zoom;
@property NSInteger tileNumber;
@property NSInteger tileSize;
@property NSString *pathPrefix;

- (id)initWithZoom:(NSInteger)zoom level:(NSInteger)level tileNumber:(NSInteger)tileNumber tileSize:(NSInteger)tileSize pathPrefix:(NSString*)pathPrefix;

@end
