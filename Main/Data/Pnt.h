//
//  Point.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-10.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pnt : NSObject

@property double lon;
@property double lat;
@property NSDate* date;
@property NSInteger x;
@property NSInteger y;

- (id) initWithDate:(NSDate*) date lon:(double)lon lat:(double)lat;
@end
