//
//  Path.m
//  Waterloo
//
//  Created by Ge Yu on 2015-02-17.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "Path.h"

@implementation Path

- (id)initWithName:(NSString*)name customized:(BOOL)customized {
  self = [super init];
  if (self) {
    self.name=name;
    self.customized=customized;
  }
  return self;
}
@end
