//
//  Level.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-07.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "Level.h"

@implementation Level
- (id)initWithZoom:(NSInteger)zoom level:(NSInteger)level tileNumber:(NSInteger)tileNumber tileSize:(NSInteger)tileSize pathPrefix:(NSString*)pathPrefix {
  self = [super init];
  if (self) {
    self.zoom=zoom;
    self.level=level;
    self.tileNumber=tileNumber;
    self.tileSize=tileSize;
    self.pathPrefix=pathPrefix;
  }
  return self;
}

- (NSComparisonResult)compare:(Level *)otherObject {
  if(self.level>otherObject.level) {
    return  (NSComparisonResult)NSOrderedDescending;
  }
  return (NSComparisonResult)NSOrderedAscending;
}


@end
