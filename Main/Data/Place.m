//
//  Spot.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-07.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "Place.h"
#import "Path.h"

@implementation Place

   - (id)init {
     return [super init];
  }

  -(id) initWithName:(NSString *)name nativeName:(NSString *)name2 longitude:(double)lon latitude:(double)lat doc:(NSString*)doc logo:(NSString*)logo index:(float)index info:(NSString*)info {
           self = [super init];
           if (self) {
             self.name=name;
             self.nativeName=name2;
             self.lon=lon;
             self.lat=lat;
             self.index=index;
             self.docPath=[[Path alloc ] initWithName:doc customized:FALSE];
             self.logoPath=[[Path alloc ] initWithName:logo customized:FALSE];;
             self.original=TRUE;
             self.info=info;
           }
    return self;
}

- (NSComparisonResult)distanceCompare:(Place *)otherObject {
  if(self.distance>otherObject.distance) {
    return  (NSComparisonResult)NSOrderedDescending;
  }
  return (NSComparisonResult)NSOrderedAscending;
}

@end
