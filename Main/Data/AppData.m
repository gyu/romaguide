//
//  Entity.m
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-07.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import "AppData.h"
#import "Level.h"

@implementation AppData
- (id)initWithCity:(NSString*)city region:(NSString*)region longitude:(double)lon latitude:(double)lat places:(NSArray *)spots levels:(NSArray *)levels info:(NSString*)info logoPath:(NSString*)logoPath {
  self = [super init];
  if (self) {
    self.city=city;
    self.region=region;
    self.lon=lon;
    self.lat=lat;
    self.places=spots;
    self.levels=levels;
    self.info=info;
    self.logoPath=logoPath;
    
    spotCount = [self.places count];
    levelCount = [self.levels count];
    currentLevel = (Level *)[self.levels objectAtIndex:0];
    
  }
  return self;
}

- (BOOL) hasPreviousLevel {
  return (currentLevel.level>0)?TRUE:FALSE;
}
- (BOOL) hasNextLevel {
  return (currentLevel.level<levelCount-1)?TRUE:FALSE;
}

- (Level *) goPrevious {
  NSInteger inx=currentLevel.level;
  if(inx>0) {
    NSInteger inx=currentLevel.level;
    currentLevel = (Level *)[self.levels objectAtIndex:inx-1];
  }
  return currentLevel;
}


- (Level *) goNext {
  NSInteger inx=currentLevel.level;
  if(inx<levelCount-1) {
    NSInteger inx=currentLevel.level;
    currentLevel = (Level *)[self.levels objectAtIndex:inx+1];
  }
  return currentLevel;
}


- (Level *) getCurrent {
  return currentLevel;
}
@end
