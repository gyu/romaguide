//
//  JsonParser.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-08.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>
@class AppData;

@interface JsonParser : NSObject
 + (AppData *) readAppData;
 + (BOOL) boolValue:(NSManagedObject *)obj key:(NSString *) key;
 + (double) doubleValue:(NSManagedObject *)obj key:(NSString *) key;
 + (float) floatValue:(NSManagedObject *)obj key:(NSString *) key;

 + (NSDictionary *) jsonData;
 + (NSString *) findDocPathAtLon:(double)lon andLat:(double)lat;
 + (NSString *) findLogoPathAtLon:(double)lon andLat:(double)lat;

@end
