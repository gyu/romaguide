//
//  Spot.h
//  LocationDetector
//
//  Created by Ge Yu on 2015-02-07.
//  Copyright (c) 2015 Ge Yu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Path;

@interface Place : NSObject {
}
@property double distance;
@property NSManagedObjectID *objId;
@property NSString *name;
@property NSString *nativeName;
@property NSString *info;
@property double lon;
@property double lat;
@property float index;
@property Path *docPath;
@property Path *logoPath;
@property NSInteger x;
@property NSInteger y;
@property BOOL original;

- (id)init;

- (id)initWithName:(NSString*)name nativeName:(NSString *)nativeName longitude:(double)lon latitude:(double)lat doc:(NSString*)doc logo:(NSString*)logo index:(float)index info:(NSString*)info;
@end
